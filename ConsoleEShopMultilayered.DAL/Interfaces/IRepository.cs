﻿using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    /// <summary>
    /// interface for generic repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// returns all elements
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// returns an element by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(int id);
        /// <summary>
        /// Finds a collection
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        /// <summary>
        /// creates a new instance
        /// </summary>
        /// <param name="item"></param>
        void Create(T item);
    }
}
