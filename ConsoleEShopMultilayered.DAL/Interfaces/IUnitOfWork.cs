﻿using ConsoleEShopMultilayered.DAL.Entities;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    /// <summary>
    /// interface of unit of work
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// 
        /// </summary>
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }
    }
}
