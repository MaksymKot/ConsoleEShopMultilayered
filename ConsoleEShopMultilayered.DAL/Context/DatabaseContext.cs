using ConsoleEShopMultilayered.DAL.Entities;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.DAL.Context
{
    public class DatabaseContext
    {
        /// <summary>
        /// List of orders in database
        /// </summary>
        public List<Order> Orders { get; set; } = new List<Order>();
        /// <summary>
        /// List of users in database
        /// </summary>
        public List<User> Users { get; set; } = new List<User>()
        {
            new User()
            {
                Name = "Bob",
                Login = "qwerty",
                Password = 123,
                Role = Enums.Roles.RegisteredUser,
            },
            new User()
            {
                Name = "John",
                Login = "azerty",
                Password = 123,
                Role = Enums.Roles.Admin,
            }
        };
        /// <summary>
        /// List of products in database
        /// </summary>
        public List<Product> Products { get; set; } = new List<Product>()
        {
            new Product() { Category = "Food", Name = "Apple", Price = 1},
            new Product() { Category = "Food", Name = "Orange", Price = 2},
            new Product() { Category = "Food", Name = "Pear", Price = 1},
        };
    }
}
