﻿namespace ConsoleEShopMultilayered.DAL.Enums
{
    /// <summary>
    /// Enum for user's role
    /// </summary>
    public enum Roles
    {
        Guest,
        RegisteredUser,
        Admin,
    }
}
