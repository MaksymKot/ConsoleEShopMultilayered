﻿namespace ConsoleEShopMultilayered.DAL.Entities
{
    /// <summary>
    /// instance of product
    /// </summary>
    public class Product
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Info { get; set; } = "No info";
        private static int _count = 0;
        public int Id { get; }

        public Product()
        {
            Id = ++_count;
        }
    }
}
