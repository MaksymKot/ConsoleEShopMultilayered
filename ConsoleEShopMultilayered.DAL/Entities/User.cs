﻿using ConsoleEShopMultilayered.DAL.Enums;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    /// <summary>
    /// instance of order
    /// </summary>
    public class User
    {
        public string Login { get; set; }
        public int Password { get; set; }
        public string Info { get; set; }
        public Roles Role { get; set; } = Roles.Guest;
        public string Name { get; set; }
        public int Id { get; set; }
        private static int count = 0;
        public User()
        {
            Id = ++count;
        }
    }

}
