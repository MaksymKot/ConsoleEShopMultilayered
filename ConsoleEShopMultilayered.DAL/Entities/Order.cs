﻿using ConsoleEShopMultilayered.DAL.Enums;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    /// <summary>
    /// instance of order
    /// </summary>
    public class Order
    {
        public int Id { get; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int UserId { get; set; }
        public OrderStatus Status { get; set; } = OrderStatus.New;
        public User User { get; set; }
        private static int count = 0;
        public Order()
        {
            Id = ++count;
        }
    }
}
