﻿using ConsoleEShopMultilayered.DAL.Context;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    /// <summary>
    /// class of unit of work
    /// </summary>
    public class UnifOfWork : IUnitOfWork
    {
        private DatabaseContext db = new DatabaseContext();
        private ProductRepository prodRepo;
        private UserRepository userRepo;
        private OrderRepository orderRepo;
        /// <summary>
        /// Dataset of users in database
        /// </summary>
        public IRepository<User> Users
        {
            get
            {
                if (userRepo is null)
                    userRepo = new UserRepository(db);
                return userRepo;
            }
        }
        /// <summary>
        /// Dataset of orders in database
        /// </summary>
        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepo is null)
                    orderRepo = new OrderRepository(db);
                return orderRepo;
            }
        }
        /// <summary>
        /// Dataset of products in database
        /// </summary>
        public IRepository<Product> Products
        {
            get
            {
                if (prodRepo is null)
                    prodRepo = new ProductRepository(db);
                return prodRepo;
            }
        }
    }
}
