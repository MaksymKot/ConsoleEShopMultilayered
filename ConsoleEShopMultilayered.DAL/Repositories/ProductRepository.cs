﻿using ConsoleEShopMultilayered.DAL.Context;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    /// <summary>
    /// repository for products
    /// </summary>
    public class ProductRepository : IRepository<Product>
    {
        private DatabaseContext db;

        public ProductRepository(DatabaseContext db)
        {
            this.db = db;
        }
        /// <summary>
        /// adds a new product to database
        /// </summary>
        /// <param name="item"></param>
        public void Create(Product item)
           => db.Products.Add(item);
        /// <summary>
        /// returns a collection of products
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>An IEnumerable collection</returns>
        public IEnumerable<Product> Find(Func<Product, bool> predicate)
            => db.Products.Where(predicate).ToList();
        /// <summary>
        /// returns a product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An instance of product</returns>
        public Product Get(int id)
            => db.Products.Find(x => x.Id == id);
        /// <summary>
        /// returns all products in database
        /// </summary>
        /// <returns>An IEnumerable collection</returns>
        public IEnumerable<Product> GetAll()
            => db.Products;
    }
}
