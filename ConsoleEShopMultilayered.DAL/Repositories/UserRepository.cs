﻿using ConsoleEShopMultilayered.DAL.Context;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    /// <summary>
    /// repository for users
    /// </summary>
    public class UserRepository : IRepository<User>
    {

        private DatabaseContext db;

        public UserRepository(DatabaseContext db)
        {
            this.db = db;
        }
        /// <summary>
        /// adds a new user to database 
        /// </summary>
        /// <param name="item"></param>
        public void Create(User item)
            => db.Users.Add(item);

        /// <summary>
        /// return a collection of users
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>An IEnumerable collection</returns>
        public IEnumerable<User> Find(Func<User, bool> predicate)
            => db.Users.Where(predicate).ToList();
        /// <summary>
        /// return an user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An instance of user</returns>
        public User Get(int id)
            => db.Users.Find(x => x.Id == id);
        /// <summary>
        /// returns a collection of all users
        /// </summary>
        /// <returns>An IEnumerable collection</returns>
        public IEnumerable<User> GetAll()
            => db.Users;
    }
}
