﻿using ConsoleEShopMultilayered.DAL.Context;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    /// <summary>
    /// repository for orders
    /// </summary>
    public class OrderRepository : IRepository<Order>
    {
        private DatabaseContext db;

        public OrderRepository(DatabaseContext db)
        {
            this.db = db;
        }
        public void Create(Order item)
            => db.Orders.Add(item);
        /// <summary>
        /// returns a collection of orders
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<Order> Find(Func<Order, bool> predicate)
            => db.Orders.Where(predicate).ToList();
        /// <summary>
        /// return an order by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Order Get(int id)
            => db.Orders.Find(x => x.Id == id);
        /// <summary>
        /// returns all orders in database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Order> GetAll()
            => db.Orders;
    }
}
