using System;
using Xunit;
using System.Linq;
using ConsoleEShopMultilayered.BLL;
using ConsoleEShopMultilayered.DAL;
using ConsoleEShopMultilayered.BLL.Services;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Enums;

namespace ConsoleEShopMultilayered
{
    public class UnitTest1
    {
        [Fact]
        public void CancelOrderWithDoesNotExistThrowException()
        {
            //arrange
            var orderService = new OrderService();
            //assert
            Assert.Throws<Exception>(() => orderService.CancelOrder(-1));
        }

        [Fact]
        public void LogInIncorrectDataThrowException()
        {
            //arrange
            var userServ = new UserService();
            var incorrectLogin = "p";
            var incorrectPassword = -1;
            //assert
            Assert.Throws<Exception>(() => userServ.LogIn(incorrectLogin, incorrectPassword));
        }

        [Fact]
        public void GetProductWhichDoesNotExistsThrowException()
        {
            //arrange
            var userServ = new UserService();
            var prodServ = new ProductService(userServ.Database, userServ.User);
            var incorrectName = "";
            //assert
            Assert.Throws<Exception>(() => prodServ.GetProduct(incorrectName));
        }

        [Fact]
        public void AddNewProduct()
        {
            //arrange
            var userServ = new UserService();
            var database = userServ.Database;
            var prodServ = new ProductService(userServ.Database, userServ.User);
            var prodName = "Potato";
            var prodPrice = 2;
            var prodInfo = "";
            var prodCategory = "Food";
            var expectedProd = new Product()
            {
                Category = prodCategory,
                Info = prodInfo,
                Name = prodName,
                Price = prodPrice,
            };
            //act
            prodServ.AddProduct(prodName, prodPrice, prodCategory, prodInfo);
            var newProd = database.Products.Find(x => x.Name.Equals(prodName)).FirstOrDefault();

            var res = newProd.Category.Equals(prodCategory) && newProd.Name.Equals(prodName)
                && prodInfo.Equals(prodInfo) && prodPrice == newProd.Price;

            //assert
            Assert.True(res);
            
        }
        [Fact]
        public void GetProductWhichExists()
        {
            //arrange
            var userServ = new UserService();
            var prodServ = new ProductService(userServ.Database, userServ.User);
            var correctName = "Apple";
            //act
            var prod = prodServ.GetProduct(correctName);
            //assert
            Assert.Equal(correctName, prod.Name);
        }

        [Fact]
        public void SuccessfullyMadeOrder()
        {
            //arrange
            var userServ = new UserService();
            var database = userServ.Database;
            var user = userServ.User;
            var correctLogin = "azerty";
            var correctPassword = 123;
            var prodServ = new ProductService(userServ.Database, user);
            var ordrServ = new OrderService(database, userServ.User);
            userServ.LogIn(correctLogin, correctPassword);
            var prodName = "Apple";
            
            //act
            var prod = prodServ.GetProduct(prodName);
            var prodId = prod.Id;
            var usrId = user.Id;

            var ordrDTO = new OrderDTO()
            {
                Product = prod,
                ProductId = prod.Id,
                UserId = user.Id,
                User = new UserDTO()
                {
                    Id = user.Id,
                    Info = user.Info,
                    Login = user.Login,
                    Name = user.Name,
                    Password = user.Password,
                    Role = user.Role,
                },
            };
            ordrServ.MakeOrder(ordrDTO);

            var ordr = database.Orders.Find(x => x.ProductId == prodId && x.UserId == usrId).FirstOrDefault();
            //assert
            Assert.Equal(ordr.Product.Name, prodName);
        }

        [Fact]
        public void SuccessfullyRegistedUser()
        {
            //arrange
            var userServ = new UserService();
            var database = userServ.Database;
            var name = "Jack";
            var passwrd = 21;
            var login = "qworty";
            //act
            userServ.Register(login, passwrd, true, name);
            var newUsr = database.Users.Find(x => x.Login.Equals(login)).FirstOrDefault();
            //assert
            Assert.Equal(name, newUsr.Name);
        }

        [Fact]
        public void LogOutPropertiesOfCurrentUser()
        {
            //arrange
            var userServ = new UserService();
            var database = userServ.Database;
            var usr = userServ.User;
            var login = "qwerty";
            var password = 123;
            //act
            userServ.LogIn(login, password);
            userServ.LogOut();
            var res = usr.Role.Equals(Roles.Guest) && usr.Password == -1
                && (usr.Name is null) && (usr.Login is null) && (usr.Info is null)
                && usr.Id == -1;
            //assert
            Assert.True(res);
        }
    }
}
