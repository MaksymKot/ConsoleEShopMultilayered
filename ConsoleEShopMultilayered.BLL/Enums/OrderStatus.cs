﻿namespace ConsoleEShopMultilayered.BLL.Enums
{
    /// <summary>
    /// Enum for order's status
    /// </summary>
    public enum OrderStatus
    {
        New,
        Done,
        Shipped,
        CancelledByAdmin,
        CancelledByUser,
        ReceivedPayment,
        Received,
        Completed,
    }
}
