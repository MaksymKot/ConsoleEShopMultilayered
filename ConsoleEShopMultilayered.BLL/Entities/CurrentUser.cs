﻿using ConsoleEShopMultilayered.DAL.Enums;

namespace ConsoleEShopMultilayered.BLL
{
    /// <summary>
    /// An intstance of current user
    /// </summary>
    public class CurrentUser
    {
        public string Login { get; set; } = default;
        public int Password { get; set; } = -1;
        public string Info { get; set; } = default;
        public Roles Role { get; set; } = Roles.Guest;
        public string Name { get; set; } = default;
        public int Id { get; set; } = -1;
    }
}
