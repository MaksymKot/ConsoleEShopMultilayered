﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Intefaces;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShopMultilayered.BLL.Services
{
    /// <summary>
    /// class of service for products
    /// </summary>
    public class ProductService : IProductService
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/>
        /// </summary>
        public IUnitOfWork Database { get; }
        /// <summary>
        /// Current user
        /// </summary>
        public CurrentUser User { get; }
        public ProductService(IUnitOfWork uow, CurrentUser user)
        {
            Database = uow;
            User = user;
        }
        /// <summary>
        /// Draws all data of products in database
        /// </summary>
        public void ViewProducts()
        {
            var products = Database.Products.GetAll();

            var productsDTO = new List<ProductDTO>();
            foreach (var item in products)
            {
                productsDTO.Add(new ProductDTO()
                {
                    Category = item.Category,
                    Id = item.Id,
                    Info = item.Info,
                    Name = item.Name,
                    Price = item.Price,
                });
            }
            foreach (var item in productsDTO)
            {
                Console.WriteLine("\n**************************\n");
                Console.WriteLine($"Name : {item.Name}");
                Console.WriteLine($"Id : {item.Id}");
                Console.WriteLine($"Price : {item.Price}");
                Console.WriteLine($"Caterogy : {item.Category}");
                Console.WriteLine($"Info : {item.Info}");
                Console.WriteLine("\n**************************\n");

            }
        }
        /// <summary>
        /// Changes products information
        /// </summary>
        /// <param name="name"></param>
        /// <param name="info"></param>
        public void ChangeProductsInfo(string name, string info)
        {
            var prod = Database.Products.Find(x => x.Name.Equals(name)).FirstOrDefault();

            if (prod is null)
                throw new Exception("Cant find this product");

            prod.Info = info;
        }
        /// <summary>
        /// returns product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An intance of product in BLL</returns>
        public ProductDTO GetProduct(int id)
        {
            var prod = Database.Products.Get(id);
            if (prod is null)
                throw new Exception();

            return new ProductDTO()
            {
                Category = prod.Category,
                Id = prod.Id,
                Info = prod.Info,
                Name = prod.Name,
                Price = prod.Price
            };
        }
        /// <summary>
        /// return product by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>An intance of product in BLL</returns>
        public ProductDTO GetProduct(string name)
        {
            var prod = Database.Products.Find(x => x.Name.Equals(name)).FirstOrDefault();

            if (prod is null)
                throw new Exception("\nCan not find this product\n");

            return new ProductDTO()
            {
                Category = prod.Category,
                Id = prod.Id,
                Info = prod.Info,
                Name = prod.Name,
                Price = prod.Price
            };
        }
        /// <summary>
        /// Adds a new product to database with unique name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="category"></param>
        /// <param name="info"></param>
        public void AddProduct(string name, int price, string category, string info)
        {
            if (name is null || price <= 0 || category is null || info is null)
                throw new Exception("\nIncorrect information about new product\n");

            var newProd = new ProductDTO()
            {
                Category = category,
                Info = info,
                Name = name,
                Price = price,
            };

            if (!(Database.Products.
                Find(x => x.Name.Equals(name)).FirstOrDefault() is null))
            {
                throw new Exception("\nThis product is already exists\n");
            }

            Database.Products.Create(new DAL.Entities.Product()
            {
                Category = newProd.Category,
                Price = newProd.Price,
                Name = newProd.Name,
                Info = newProd.Info,
            });
        }
    }
}
