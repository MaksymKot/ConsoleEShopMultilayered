﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Intefaces;
using ConsoleEShopMultilayered.DAL.Enums;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Linq;

namespace ConsoleEShopMultilayered.BLL.Services
{
    /// <summary>
    /// class of service for users
    /// </summary>
    public class UserService : IUserService
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/>
        /// </summary>
        public IUnitOfWork Database { get; }
        /// <summary>
        /// current user
        /// </summary>
        public CurrentUser User { get; }

        public UserService(IUnitOfWork uow, CurrentUser user)
        {
            Database = uow;
            User = user;
        }
        public UserService()
        {
            Database = new DAL.Repositories.UnifOfWork();
            User = new CurrentUser();
        }
        /// <summary>
        /// This method adds to database a new user with unique login
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="asAdmin"></param>
        /// <param name="name"></param>
        public void Register(string login, int password, bool asAdmin, string name)
        {
            var newUser = new UserDTO()
            {
                Login = login,
                Password = password,
                Role = asAdmin ? Roles.Admin : Roles.RegisteredUser,
                Name = name,
            };

            if (!(Database.Users.Find(x => x.Login.Equals(login)).FirstOrDefault() is null))
                throw new Exception("\nThis login is already taken\n");

            Database.Users.Create(new DAL.Entities.User()
            {
                Name = newUser.Name,
                Info = newUser.Info,
                Login = newUser.Login,
                Role = newUser.Role,
                Password = newUser.Password,
            });
        }
        /// <summary>
        /// Changes personal information of registed user
        /// </summary>
        /// <param name="info"></param>
        /// <param name="name"></param>
        public void ChangeInfo(string info, string name)
        {
            ChangeUsersInfo(User.Login, info, name);
        }
        /// <summary>
        /// Chamges personal information of user
        /// </summary>
        /// <param name="login"></param>
        /// <param name="info"></param>
        /// <param name="name"></param>
        public void ChangeUsersInfo(string login, string info, string name)
        {
            var user = Database.Users.Find(x => x.Login.Equals(login)).FirstOrDefault();

            if (user is null)
                throw new Exception("\nCan not find this user\n");

            user.Info = info;
            user.Name = name;

        }
        /// <summary>
        /// returns user
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public UserDTO GetUser(string login)
        {
            var user = Database.Users.Find(x => x.Login.Equals(login)).FirstOrDefault();

            if (user is null)
                throw new Exception("\nCan not find this user\n");

            return new UserDTO()
            {
                Id = user.Id,
                Info = user.Info,
                Name = user.Name,
                Login = user.Login,
                Password = user.Password,
                Role = user.Role,
            };
        }
        /// <summary>
        /// Changed data of current user 
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public void LogIn(string login, int password)
        {
            var user_db = Database.Users.Find(x => x.Login.Equals(login)).FirstOrDefault();

            if (user_db is null)
                throw new Exception("\nCan not find this user\n");

            var userDTO = new UserDTO()
            {
                Login = user_db.Login,
                Id = user_db.Id,
                Info = user_db.Info,
                Password = user_db.Password,
                Name = user_db.Name,
                Role = user_db.Role,
            };

            if (userDTO.Password == password)
            {
                User.Password = userDTO.Password;
                User.Role = userDTO.Role;
                User.Name = userDTO.Name;
                User.Login = userDTO.Login;
                User.Info = userDTO.Info;
                User.Id = userDTO.Id;
            }
            else throw new Exception("\nIncorrect password\n");
        }
        /// <summary>
        /// Changed data of current user to default.
        /// </summary>
        public void LogOut()
        {
            User.Id = -1;
            User.Info = default;
            User.Role = Roles.Guest;
            User.Password = -1;
            User.Name = default;
            User.Login = default;
        }
    }
}
