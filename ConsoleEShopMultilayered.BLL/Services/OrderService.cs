﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Intefaces;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Enums;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShopMultilayered.BLL.Services
{
    /// <summary>
    /// class of service for orders
    /// </summary>
    public class OrderService : IOrderService
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/>
        /// </summary>
        public IUnitOfWork Database { get; }
        /// <summary>
        /// current user
        /// </summary>
        public CurrentUser User { get; }
        public OrderService(IUnitOfWork uow, CurrentUser user)
        {
            if (uow is null || user is null)
                throw new Exception("\nError\n");
            Database = uow;
            this.User = user;
        }

        public OrderService()
        {
            Database = new DAL.Repositories.UnifOfWork();
        }
        /// <summary>
        /// Makes a new order with unique id
        /// </summary>
        /// <param name="orderDTO"></param>
        public void MakeOrder(OrderDTO orderDTO)
        {
            if (orderDTO is null)
                throw new Exception("\nCan't make an order\n");

            var Prod = Database.Products.Get(orderDTO.ProductId);

            if (Prod is null)
                throw new Exception("\nCan not find the product\n");

            var Order = new Order()
            {
                ProductId = Prod.Id,
                Product = Prod,
                UserId = User.Id,
                User = new User()
                {
                    Login = User.Login,
                    Name = User.Name,
                    Info = User.Info,
                    Password = User.Password,
                    Role = User.Role,
                    Id = User.Id,
                }
            };
            Database.Orders.Create(Order);
        }
        /// <summary>
        /// cancels order
        /// </summary>
        /// <param name="id"></param>
        public void CancelOrder(int id)
        {
            var order = Database.Orders.Find(x => x.Id == id && x.UserId == User.Id).FirstOrDefault();

            if (order is null)
                throw new Exception("\nCant find this order\n");

            else
            {
                if (order.Status.Equals(OrderStatus.New))
                {
                    order.Status = User.Role.Equals(Roles.RegisteredUser)
                        ? OrderStatus.CancelledByUser : OrderStatus.CancelledByAdmin;
                }
                else throw new Exception("\nCan not cancel this order\n");
            }
        }
        /// <summary>
        /// changes a status of all user's orders to "Received payment"
        /// </summary>
        public void CheckOut()
        {
            var orders = Database.Orders.Find(x => x.UserId == User.Id &&
                x.Status.Equals(DAL.Enums.OrderStatus.New));
            if (orders is null)
                throw new Exception("Do dont have orders with status \"New\"");
            else foreach (var item in orders) item.Status = OrderStatus.ReceivedPayment;
        }
        /// <summary>
        /// Changes a status of an order
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        public void SetStatusToOrder(int id, string status)
        {
            var order = Database.Orders.Find(x => x.Id == id).FirstOrDefault();

            if (order is null)
                throw new Exception("\nCant find this order\n");

            Console.WriteLine("\nAvailable statuses:\n");
            Console.WriteLine("Shipped");
            Console.WriteLine("Cancelled by admin");
            Console.WriteLine("Received");
            Console.WriteLine("Done");
            Console.WriteLine("Received payment\n");

            if (status.Equals(""))
            {
                Console.WriteLine("\nEnter new status of order\n");
                status = Console.ReadLine();
            }

            switch (status)
            {
                case "Shipped":
                    {
                        if (!order.Status.Equals(OrderStatus.CancelledByUser)
                            && !order.Status.Equals(OrderStatus.CancelledByAdmin)
                            && !order.Status.Equals(OrderStatus.Done)
                            && !order.Status.Equals(OrderStatus.Received)
                            && order.Status.Equals(OrderStatus.ReceivedPayment)
                            && !order.Status.Equals(OrderStatus.New))
                        {
                            order.Status = OrderStatus.Shipped;
                            Console.WriteLine("\nOrder's status has been changed\n");
                        }
                        else Console.WriteLine("\nCan't do this\n");
                        break;
                    }
                case "Received":
                    {
                        if (!order.Status.Equals(OrderStatus.CancelledByUser)
                            && !order.Status.Equals(OrderStatus.CancelledByAdmin)
                            && !order.Status.Equals(OrderStatus.Done)
                            && !order.Status.Equals(OrderStatus.ReceivedPayment)
                            && order.Status.Equals(OrderStatus.Shipped))
                        {
                            order.Status = OrderStatus.Received;
                            Console.WriteLine("\nOrder's status has been changed\n");
                        }
                        else Console.WriteLine("\nCan't do this\n");
                        break;
                    }
                case "Cancelled by user":
                    {
                        if (order.Status.Equals(OrderStatus.New) && User.Role.Equals(Roles.RegisteredUser))
                        {
                            order.Status = OrderStatus.CancelledByUser;
                            Console.WriteLine("\nOrder's status has been changed\n");
                        }
                        else Console.WriteLine("\nCan't do this\n");
                        break;
                    }
                case "Cancelled by admin":
                    {
                        if (User.Role.Equals(Roles.Admin) && !order.Status.Equals(OrderStatus.Done))
                        {
                            order.Status = OrderStatus.CancelledByAdmin;
                            Console.WriteLine("\nOrder's status has been changed\n");
                        }
                        else Console.WriteLine("\nCan't do this\n");
                        break;
                    }
                case "Done":
                    {
                        if (order.Status.Equals(OrderStatus.Received))
                        {
                            order.Status = OrderStatus.Done;
                            Console.WriteLine("\nOrder's status has been changed\n");
                        }
                        break;
                    }
                case "Received payment":
                    {
                        if (order.Status.Equals(OrderStatus.New))
                        {
                            order.Status = OrderStatus.ReceivedPayment;
                            Console.WriteLine("\nOrder's status has been changed\n");
                        }
                        else Console.WriteLine("\nCan't do this\n");
                        break;
                    }
                default: throw new Exception("\nIncorrect status\n");
            }
        }
        /// <summary>
        /// Draws a order history of current user
        /// </summary>
        public void SeeOrderHistory()
        {
            var orders = Database.Orders.Find(x => x.UserId == User.Id);

            if (orders is null)
                throw new Exception("\nThis user does not have any order\n");

            var ordersDTO = new List<OrderDTO>();

            foreach (var item in orders)
            {
                ordersDTO.Add(new OrderDTO()
                {
                    Id = item.Id,
                    Product = new ProductDTO()
                    {
                        Id = item.Product.Id,
                        Category = item.Product.Category,
                        Info = item.Product.Info,
                        Name = item.Product.Name,
                        Price = item.Product.Price,
                    },
                    ProductId = item.ProductId,
                    Status = (Enums.OrderStatus)item.Status,
                    User = new UserDTO()
                    {
                        Name = item.User.Name,
                        Info = item.User.Info,
                        Id = item.User.Id,
                        Login = item.User.Login,
                        Password = item.User.Password,
                        Role = item.User.Role,
                    },
                    UserId = item.UserId,
                });
            }

            if (ordersDTO is null)
                throw new Exception("\nCant find order history\n");
            if (ordersDTO.Count == 0) Console.WriteLine("You have not any order");
            else
            {
                foreach (var item in ordersDTO)
                {
                    Console.WriteLine("\n**************\n");
                    Console.WriteLine($"Status :  {item.Status}");
                    Console.WriteLine($"Id : {item.Id}");
                    Console.WriteLine($"Name :  {item.Product.Name}");
                    Console.WriteLine($"Price :  {item.Product.Price}");
                    Console.WriteLine($"Info :  {item.Product.Info}");
                    Console.WriteLine($"Category :  {item.Product.Category}");
                    Console.WriteLine("\n**************\n\n");
                }
            }
        }
    }
}
