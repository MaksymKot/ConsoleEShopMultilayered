﻿namespace ConsoleEShopMultilayered.BLL.DTO
{
    /// <summary>
    /// instance of product in BLL layer
    /// </summary>
    public class ProductDTO
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Info { get; set; } = "No info";
        public int Id { get; set; }
    }
}
