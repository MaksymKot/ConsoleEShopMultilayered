﻿using ConsoleEShopMultilayered.DAL.Enums;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    /// <summary>
    /// instance of user in BLL layer
    /// </summary>
    public class UserDTO
    {
        public string Login { get; set; }
        public int Password { get; set; }
        public string Info { get; set; }
        public Roles Role { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
