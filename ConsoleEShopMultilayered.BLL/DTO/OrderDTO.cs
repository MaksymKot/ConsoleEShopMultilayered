﻿using ConsoleEShopMultilayered.BLL.Enums;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    /// <summary>
    /// instance of order in BLL layer
    /// </summary>
    public class OrderDTO
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public ProductDTO Product { get; set; }
        public OrderStatus Status { get; set; }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
    }
}
