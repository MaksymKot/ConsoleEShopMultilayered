﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Interfaces;

namespace ConsoleEShopMultilayered.BLL.Intefaces
{
    /// <summary>
    /// interfasce of service for products
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// database
        /// </summary>
        public IUnitOfWork Database { get; }
        /// <summary>
        /// current user
        /// </summary>
        public CurrentUser User { get; }
        /// <summary>
        /// draws all informayion about products in database
        /// </summary>
        void ViewProducts();
        /// <summary>
        /// changes product's information
        /// </summary>
        /// <param name="name"></param>
        /// <param name="info"></param>
        void ChangeProductsInfo(string name, string info);
        /// <summary>
        /// returns a product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An intance of product in BLL</returns>
        ProductDTO GetProduct(int id);
        /// <summary>
        /// returns a product by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns><An intance of product in BLL/returns>
        ProductDTO GetProduct(string name);
        /// <summary>
        /// adds a new product with an unique name in database 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="category"></param>
        /// <param name="info"></param>
        void AddProduct(string name, int price, string category, string info);
    }
}
