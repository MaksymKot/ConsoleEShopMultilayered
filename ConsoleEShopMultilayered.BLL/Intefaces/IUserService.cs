﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Interfaces;

namespace ConsoleEShopMultilayered.BLL.Intefaces
{
    /// <summary>
    /// interfasce of service for users
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// database
        /// </summary>
        public IUnitOfWork Database { get; }
        /// <summary>
        /// current user
        /// </summary>
        public CurrentUser User { get; }
        /// <summary>
        /// adds a new user to database
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="asAdmin"></param>
        /// <param name="name"></param>
        void Register(string login, int password, bool asAdmin, string name);
        /// <summary>
        /// changes user's info
        /// </summary>
        /// <param name="login"></param>
        /// <param name="info"></param>
        /// <param name="name"></param>
        void ChangeUsersInfo(string login, string info, string name);
        /// <summary>
        /// Changes data of cuurent user
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        void LogIn(string login, int password);
        /// <summary>
        /// Changes data of cuurent user to default
        /// </summary>
        void LogOut();
        /// <summary>
        /// Returns an user
        /// </summary>
        /// <param name="login"></param>
        /// <returns>An intance of user in BLL</returns>
        UserDTO GetUser(string login);
        /// <summary>
        /// Changes info of current user
        /// </summary>
        /// <param name="info"></param>
        /// <param name="name"></param>
        void ChangeInfo(string info, string name);
    }
}
