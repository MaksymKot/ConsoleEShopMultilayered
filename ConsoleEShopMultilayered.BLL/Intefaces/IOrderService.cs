﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Interfaces;

namespace ConsoleEShopMultilayered.BLL.Intefaces
{
    /// <summary>
    /// interfasce of service for orders
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// database
        /// </summary>
        public IUnitOfWork Database { get; }
        /// <summary>
        /// current user
        /// </summary>
        public CurrentUser User { get; }
        /// <summary>
        /// makes a new order with unique id
        /// </summary>
        /// <param name="orderDTO"></param>
        void MakeOrder(OrderDTO orderDTO);
        /// <summary>
        /// cancels an order
        /// </summary>
        /// <param name="id"></param>
        void CancelOrder(int id);
        /// <summary>
        /// changes status of order
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        void SetStatusToOrder(int id, string status);
        /// <summary>
        /// Changes statuses of all orders of current user to "Received Payment"
        /// </summary>
        void CheckOut();
        /// <summary>
        /// Draws an order history of current user
        /// </summary>
        void SeeOrderHistory();
    }
}
