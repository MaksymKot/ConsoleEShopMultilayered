﻿using ConsoleEShopMultilayered.BLL;
using ConsoleEShopMultilayered.BLL.Intefaces;
using ConsoleEShopMultilayered.BLL.Services;
using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.MenuCommands
{
    public class GuestCommands
    {
        private List<Action> lst = new List<Action>();
        /// <summary>
        /// an instance of service for users
        /// </summary>
        public readonly IUserService userService;
        /// <summary>
        /// an instance of service for products
        /// </summary>
        private readonly IProductService prodService;
        /// <summary>
        /// an instance of current user
        /// </summary>
        public CurrentUser user;
        /// <summary>
        /// a list of commands
        /// </summary>
        public Action[] commands { get; }

        public GuestCommands()
        {
            userService = new UserService();
            user = userService.User;
            prodService = new ProductService(userService.Database, user);
            GenerateGuestCommands();
            commands = lst.ToArray();
        }
        /// <summary>
        /// This method generated the list of commands for guests
        /// </summary>
        private void GenerateGuestCommands()
        {
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\t Guest's menu:");
                Console.WriteLine("1) - View all products");
                Console.WriteLine("2) - Find product by name");
                Console.WriteLine("3) - Register");
                Console.WriteLine("4) - Log in");
                Console.WriteLine("5) - Exit");
                Console.WriteLine("\n Please make a choice\n");

            }));
            //1
            lst.Add(new Action(() => prodService.ViewProducts()));
            //2
            lst.Add(new Action(() =>
            {
                Console.WriteLine("Enter a name of product:");
                var name = Console.ReadLine();
                var prod = prodService.GetProduct(name);
                Console.WriteLine("\nInfo about product:");
                Console.WriteLine($"Name : {prod.Name}");
                Console.WriteLine($"Price : {prod.Price}");
                Console.WriteLine($"Category : {prod.Category}");
                Console.WriteLine($"Info : {prod.Info}");
                Console.WriteLine($"Id : {prod.Id}");
                Console.WriteLine();
            }));
            //3
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter the login\n");
                var login = Console.ReadLine();
                Console.WriteLine("\nEnter the password\n");
                var password = int.Parse(Console.ReadLine());
                Console.WriteLine("\nEnter the name of user\n");
                var name = Console.ReadLine();
                Console.WriteLine("\nRegister as admin?\n");
                var answr = Console.ReadLine();
                bool admin = false;
                if (answr.ToUpper().Equals("Y") || answr.Equals("Y"))
                    admin = true;
                if (answr.ToUpper().Equals("N") || answr.Equals("N"))
                    admin = false;
                userService.Register(login, password, admin, name);
            }));
            //4
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter the login\n");
                var login = Console.ReadLine();
                Console.WriteLine("\nEnter the password\n");
                var password = int.Parse(Console.ReadLine());
                userService.LogIn(login, password);
            }));

        }
    }
}
