﻿using ConsoleEShopMultilayered.BLL;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Intefaces;
using ConsoleEShopMultilayered.BLL.Services;
using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.MenuCommands
{
    public class AdminCommands
    {
        private List<Action> lst = new List<Action>();
        /// <summary>
        /// an instance of service for users
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// an instance of service for products
        /// </summary>
        private readonly IProductService prodService;
        /// <summary>
        /// an instance of service for orders
        /// </summary>
        private readonly IOrderService orderService;
        /// <summary>
        /// an instance of current user
        /// </summary>
        public CurrentUser user;
        public Action[] commands { get; }

        public AdminCommands(IUserService srv)
        {
            userService = srv;
            user = userService.User;
            prodService = new ProductService(srv.Database, user);
            orderService = new OrderService(srv.Database, user);
            GenerateAdminCommands();
            commands = lst.ToArray();
        }
        /// <summary>
        /// This method generated commands for admins
        /// </summary>
        private void GenerateAdminCommands()
        {
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\n\t Admin's menu:");
                Console.WriteLine("1) - View all products");
                Console.WriteLine("2) - Find product by name");
                Console.WriteLine("3) - Make order");
                Console.WriteLine("4) - Checkout order");
                Console.WriteLine("5) - See order history");
                Console.WriteLine("6) - See user's info");
                Console.WriteLine("7) - Change user's info");
                Console.WriteLine("8) - Add new product");
                Console.WriteLine("9) - Change product's info");
                Console.WriteLine("10) - Set status to order");
                Console.WriteLine("11) - Log out");
                Console.WriteLine("\n Please make a choice\n");
            }));
            //1
            lst.Add(new Action(() => prodService.ViewProducts()));
            //2
            lst.Add(new Action(() =>
            {
                Console.WriteLine("Enter a name of product:");
                var name = Console.ReadLine();
                var prod = prodService.GetProduct(name);
                Console.WriteLine("\nInfo about product:");
                Console.WriteLine($"Name : {prod.Name}");
                Console.WriteLine($"Price : {prod.Price}");
                Console.WriteLine($"Category : {prod.Category}");
                Console.WriteLine($"Info : {prod.Info}");
                Console.WriteLine($"Id : {prod.Id}");
                Console.WriteLine();
            }));
            //3
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter the name of product which you are looking for\n");
                string prod_name = Console.ReadLine();
                var b = prodService.GetProduct(prod_name);
                var c = new OrderDTO()
                {
                    Product = b,
                    ProductId = b.Id,
                    UserId = user.Id,
                    User = new UserDTO()
                    {
                        Id = user.Id,
                        Info = user.Info,
                        Login = user.Login,
                        Name = user.Name,
                        Password = user.Password,
                        Role = user.Role,
                    },
                };
                orderService.MakeOrder(c);
            }));
            //4
            lst.Add(new Action(() => orderService.CheckOut()));
            //5
            lst.Add(new Action(() => orderService.SeeOrderHistory()));
            //6
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter user's login\n");
                var login = Console.ReadLine();
                var usr = userService.GetUser(login);
                Console.WriteLine("\nUser's info\n");
                Console.WriteLine($"Login : {usr.Login}");
                Console.WriteLine($"Name : {usr.Name}");
                Console.WriteLine($"Id : {usr.Id}");
                Console.WriteLine($"Role : {usr.Role}");
                Console.WriteLine($"Info : {usr.Info}");
                Console.WriteLine($"Password : {usr.Password}\n");
            }));
            //7
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter user's login\n");
                var login = Console.ReadLine();
                Console.WriteLine("\nEnter new user's info\n");
                var info = Console.ReadLine();
                Console.WriteLine("\nEnter new user's name\n");
                var name = Console.ReadLine();
                userService.ChangeUsersInfo(login, info, name);
            }));
            //8
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter name of new product\n");
                var prod_name = Console.ReadLine();
                Console.WriteLine("\nEnter price of new product\n");
                var prod_price = int.Parse(Console.ReadLine());
                Console.WriteLine("\nEnter category of new  product\n");
                var prod_category = Console.ReadLine();
                Console.WriteLine("\nEnter info to new product\n");
                var prod_info = Console.ReadLine();
                prodService.AddProduct(prod_name, prod_price, prod_category, prod_info);
            }));
            //9
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter name of product\n");
                var name_prod = Console.ReadLine();
                Console.WriteLine("\nEnter new info of product\n");
                var info_prod = Console.ReadLine();
                prodService.ChangeProductsInfo(name_prod, info_prod);
            }));
            //10
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter id of order\n");
                var order_id = int.Parse(Console.ReadLine());
                orderService.SetStatusToOrder(order_id, "");
            }));
            //11
            lst.Add(new Action(() => userService.LogOut()));

        }
    }
}
