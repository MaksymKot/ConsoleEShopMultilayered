﻿using ConsoleEShopMultilayered.BLL;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Intefaces;
using ConsoleEShopMultilayered.BLL.Services;
using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.MenuCommands
{
    public class RegisteredUserCommands
    {
        private List<Action> lst = new List<Action>();
        /// <summary>
        /// an instance of service for users
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// an instance of service for products
        /// </summary>
        private readonly IProductService prodService;
        /// <summary>
        /// an instance of service for orders
        /// </summary>
        private readonly IOrderService orderService;
        /// <summary>
        /// an instance of current user
        /// </summary>
        public CurrentUser user;
        /// <summary>
        /// a list of commands
        /// </summary>
        public Action[] commands { get; }

        public RegisteredUserCommands(IUserService srv)
        {
            userService = srv;
            user = userService.User;
            prodService = new ProductService(srv.Database, user);
            orderService = new OrderService(srv.Database, user);
            GenerateRegUserCommands();
            commands = lst.ToArray();
        }
        /// <summary>
        /// This method generated the list of command for registed users
        /// </summary>
        private void GenerateRegUserCommands()
        {
            //0
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\n\t User's menu:");
                Console.WriteLine("1) - View all products");
                Console.WriteLine("2) - Find product by name");
                Console.WriteLine("3) - Make order");
                Console.WriteLine("4) - Checkout order");
                Console.WriteLine("5) - See order history");
                Console.WriteLine("6) - Cancel your order");
                Console.WriteLine("7) - See order's status");
                Console.WriteLine("8) - Change order's status to \"Received\"");
                Console.WriteLine("9) - Change your personal info");
                Console.WriteLine("10) - log out");
                Console.WriteLine("\n Please make a choice\n");
            }));
            //1
            lst.Add(new Action(() => prodService.ViewProducts()));
            //2
            lst.Add(new Action(() =>
            {
                Console.WriteLine("Enter a name of product:");
                var name = Console.ReadLine();
                var prod = prodService.GetProduct(name);
                Console.WriteLine("\nInfo about product:");
                Console.WriteLine($"Name : {prod.Name}");
                Console.WriteLine($"Price : {prod.Price}");
                Console.WriteLine($"Category : {prod.Category}");
                Console.WriteLine($"Info : {prod.Info}");
                Console.WriteLine($"Id : {prod.Id}");
                Console.WriteLine();
            }));
            //3 
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter the name of product which you are looking for\n");
                string prod_name = Console.ReadLine();
                var b = prodService.GetProduct(prod_name);
                var c = new OrderDTO()
                {
                    Product = b,
                    ProductId = b.Id,
                    UserId = user.Id,
                    User = new UserDTO()
                    {
                        Id = user.Id,
                        Info = user.Info,
                        Login = user.Login,
                        Name = user.Name,
                        Password = user.Password,
                        Role = user.Role,
                    },
                };

                orderService.MakeOrder(c);
            }));
            //4
            lst.Add(new Action(() => orderService.CheckOut()));
            //5
            lst.Add(new Action(() => orderService.SeeOrderHistory()));
            //6
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter id of your order\n");
                var id = int.Parse(Console.ReadLine());
                orderService.CancelOrder(id);
            }));
            //7
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter id of  order\n");
                var id_order = int.Parse(Console.ReadLine());
                var order = orderService.Database.Orders.Get(id_order);
                Console.WriteLine($"\nOrder's status : {order.Status}\n");
            }));
            //8 
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter id of your searching product\n");
                var order_id = int.Parse(Console.ReadLine());
                orderService.SetStatusToOrder(order_id, "Received");
            }));
            //9
            lst.Add(new Action(() =>
            {
                Console.WriteLine("\nEnter new name\n");
                string new_name = Console.ReadLine();
                Console.WriteLine("\nEnter new info\n");
                string new_info = Console.ReadLine();
                userService.ChangeInfo(new_info, new_name);
            }));
            //10
            lst.Add(new Action(() => userService.LogOut()));
        }
    }
}
