﻿using ConsoleEShopMultilayered.BLL;
using ConsoleEShopMultilayered.MenuCommands;
using System;

namespace ConsoleEShopMultilayered
{
    /// <summary>
    /// This class makes interface of shop's menu
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// a set of commands for registed users
        /// </summary>
        private readonly RegisteredUserCommands registeredUserCommand;
        /// <summary>
        /// a set of commands for guests
        /// </summary>
        private readonly GuestCommands guestCommand;
        /// <summary>
        /// a set of command for admins
        /// </summary>
        private readonly AdminCommands adminCommand;
        /// <summary>
        /// instance of current user
        /// </summary>
        private readonly CurrentUser user;

        public Menu()
        {
            guestCommand = new GuestCommands();
            user = guestCommand.user;
            adminCommand = new AdminCommands(guestCommand.userService);
            registeredUserCommand = new RegisteredUserCommands(guestCommand.userService);
        }
        /// <summary>
        /// This method draws a menu
        /// </summary>
        public void Start()
        {
            bool exit = false;
            int answer = -1;
            int OptionToExit = guestCommand.commands.Length;

            while (!exit)
            {
                switch (user.Role)
                {
                    case DAL.Enums.Roles.Guest:
                        {
                            try
                            {
                                guestCommand.commands[0].Invoke();
                                answer = int.Parse(Console.ReadLine());
                                while (answer < 1 || answer > guestCommand.commands.Length - 1)
                                {
                                    bool flag = false;
                                    if (answer == OptionToExit)
                                    {
                                        exit = true;
                                        flag = true;
                                    }
                                    else
                                    {
                                        Console.WriteLine("\nChoise the correct command\n");
                                        answer = int.Parse(Console.ReadLine());
                                    }

                                    if (flag) break;
                                }
                                if (!exit)
                                    guestCommand.commands[answer].Invoke();
                            }
                            catch (Exception e) { Console.WriteLine(e.Message); }
                            break;
                        }
                    case DAL.Enums.Roles.RegisteredUser:
                        {
                            try
                            {
                                registeredUserCommand.commands[0].Invoke();
                                answer = int.Parse(Console.ReadLine());
                                while (answer < 1 || answer > registeredUserCommand.commands.Length)
                                {
                                    Console.WriteLine("Choise the correct option");
                                    answer = int.Parse(Console.ReadLine());
                                }

                                registeredUserCommand.commands[answer].Invoke();
                            }
                            catch (Exception e) { Console.WriteLine(e.Message); }
                            break;
                        }
                    case DAL.Enums.Roles.Admin:
                        {
                            try
                            {
                                adminCommand.commands[0].Invoke();
                                answer = int.Parse(Console.ReadLine());
                                while (answer < 1 || answer > adminCommand.commands.Length)
                                {
                                    Console.WriteLine("Choise the correct option");
                                    answer = int.Parse(Console.ReadLine());
                                }
                                adminCommand.commands[answer].Invoke();
                            }
                            catch (Exception e) { Console.WriteLine(e.Message); }
                            break;
                        }
                }
            }
            Console.WriteLine("\nShutting down\n");
        }
    }
}
